# NOTE:

Download Assets Packs from the Unreal Marketplace. And copy the “\Content\RTUPacks\MODs\” folder of downloaded  RTUPacks into 

the same folder of this Demo Project. If a conflict occurs, use the downloaded assets file first.

# RealToUnrealPacks

## RTUPack1-1

**README**: [README_RTUPack1-1](README/README_RTUPack1-1.md)

**UE Marketplace:** https://www.unrealengine.com/marketplace/en-US/product/rtupack1-1-contains-10-photorealistic-photogrammetry-statues

**Demo Video:** https://youtu.be/vstQTCaSaLA

![THUMBNAIL_284x284](README/README_RTUPack1-1/00_Res/01_Images/THUMBNAIL_284x284.png)

### Showcases

![FEATURED IMAGE 894x488](README/README_RTUPack1-1/00_Res/01_Images/FEATURED IMAGE 894x488.png)



## RTUPack1-2

**README**: [README_RTUPack1-2](README/README_RTUPack1-2.md)

**UE Marketplace:**https://www.unrealengine.com/marketplace/en-US/product/67541dfb8ee342009dbbbedda059dcd6

**Demo Video:** https://youtu.be/ckdQgrmTdrI

![THUMBNAIL_284x284](README/00_Res/01_Images/THUMBNAIL_284x284-16562576768152.png)

### Showcases

![FEATURED_IMAGE_894x488](README/00_Res/01_Images/FEATURED_IMAGE_894x488-16604848001757.png)

## RTUPack1-3

**README**: [README_RTUPack1-3](README/README_RTUPack1-3.md)

**UE Marketplace:**https://www.unrealengine.com/marketplace/en-US/product/rtupack1-3-contains-10-photorealistic-photogrammetry-game-ready-models

**Demo Video:** https://youtu.be/WDlrZRPheo0

![THUMBNAIL_284x284](README/README_RTUPack1-3/00_Res/01_Images/THUMBNAIL_284x284.png)

### Showcases

![FEATURED_IMAGE_894x488](README/00_Res/01_Images/FEATURED_IMAGE_894x488-16604847673305.png)

## RTUPack1-4

**README**: [README_RTUPack1-4](README/README_RTUPack1-4.md)

**UE Marketplace:**https://www.unrealengine.com/marketplace/en-US/product/rtupack1-4-contains-10-photorealistic-photogrammetry-game-ready-bird-models

![THUMBNAIL_284x284](README/README_RTUPack1-4/00_Res/01_Images/THUMBNAIL_284x284.png)

### Showcases

![FEATURED_IMAGE_894x488-16604846993202](README/00_Res/01_Images/FEATURED_IMAGE_894x488-16604846993202.png)

