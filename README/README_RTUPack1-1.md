# NOTE:

Download Assets Packs from the Unreal Marketplace. And copy the “\Content\RTUPacks\MODs\” folder of downloaded  RTUPacks into 

the same folder of this Demo Project. If a conflict occurs, use the downloaded assets file first.

**UE Marketplace:** https://www.unrealengine.com/marketplace/en-US/product/rtupack1-1-contains-10-photorealistic-photogrammetry-statues

**Video Demo:** https://youtu.be/vstQTCaSaLA

# RealToUnrealPacks

## RTUPack1-1

**UE Marketplace:** https://www.unrealengine.com/marketplace/en-US/product/rtupack1-1-contains-10-photorealistic-photogrammetry-statues

**Demo Video:** https://youtu.be/vstQTCaSaLA

![THUMBNAIL_284x284](README_RTUPack1-1/00_Res/01_Images/THUMBNAIL_284x284.png)

### Showcases

![FEATURED IMAGE 894x488](README_RTUPack1-1/00_Res/01_Images/FEATURED IMAGE 894x488.png)

#### Images

![00_01](README_RTUPack1-1/00_Res/01_Images/00_01.jpg)

![00_02](README_RTUPack1-1/00_Res/01_Images/00_02.jpg)

![01_01](README_RTUPack1-1/00_Res/01_Images/01_01.jpg)

![01_02](README_RTUPack1-1/00_Res/01_Images/01_02.jpg)

![02_01](README_RTUPack1-1/00_Res/01_Images/02_01.jpg)

![02_02](README_RTUPack1-1/00_Res/01_Images/02_02.jpg)

![03_01](README_RTUPack1-1/00_Res/01_Images/03_01.jpg)

![03_02](README_RTUPack1-1/00_Res/01_Images/03_02.jpg)

![04_01](README_RTUPack1-1/00_Res/01_Images/04_01.jpg)

![04_02](README_RTUPack1-1/00_Res/01_Images/04_02.jpg)

![05_01](README_RTUPack1-1/00_Res/01_Images/05_01.jpg)

![05_02](README_RTUPack1-1/00_Res/01_Images/05_02.jpg)

![06_01](README_RTUPack1-1/00_Res/01_Images/06_01.jpg)

![06_02](README_RTUPack1-1/00_Res/01_Images/06_02.jpg)

![07_01](README_RTUPack1-1/00_Res/01_Images/07_01.jpg)

![07_02](README_RTUPack1-1/00_Res/01_Images/07_02.jpg)

![08_01](README_RTUPack1-1/00_Res/01_Images/08_01.jpg)

![08_02](README_RTUPack1-1/00_Res/01_Images/08_02.jpg)

![09_01](README_RTUPack1-1/00_Res/01_Images/09_01.jpg)

![09_02](README_RTUPack1-1/00_Res/01_Images/09_02.jpg)

![10_01](README_RTUPack1-1/00_Res/01_Images/10_01.jpg)

![10_02](README_RTUPack1-1/00_Res/01_Images/10_02.jpg)