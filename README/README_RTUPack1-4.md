# NOTE:

Download Assets Packs from the Unreal Marketplace. And copy the “\Content\RTUPacks\MODs\” folder of downloaded  RTUPacks into 

the same folder of this Demo Project. If a conflict occurs, use the downloaded assets file first.

# RealToUnrealPacks

## RTUPack1-4

**UE Marketplace:**https://www.unrealengine.com/marketplace/en-US/product/rtupack1-4-contains-10-photorealistic-photogrammetry-game-ready-bird-models

![THUMBNAIL_284x284](README_RTUPack1-4/00_Res/01_Images/THUMBNAIL_284x284.png)

### Showcases

![FEATURED_IMAGE_894x488](README_RTUPack1-4/00_Res/01_Images/FEATURED_IMAGE_894x488.png)

#### Images

![02-02](README_RTUPack1-4/00_Res/01_Images/02-02.jpg)

![02-03](README_RTUPack1-4/00_Res/01_Images/02-03.jpg)

![02-04.jpg](README_RTUPack1-4/00_Res/01_Images/02-04.jpg.png)

![03-01](README_RTUPack1-4/00_Res/01_Images/03-01.jpg)

![03-02](README_RTUPack1-4/00_Res/01_Images/03-02.jpg)

![04-01](README_RTUPack1-4/00_Res/01_Images/04-01.jpg)

![04-02](README_RTUPack1-4/00_Res/01_Images/04-02.jpg)

![05-01](README_RTUPack1-4/00_Res/01_Images/05-01.jpg)

![05-02](README_RTUPack1-4/00_Res/01_Images/05-02.jpg)

![06-01](README_RTUPack1-4/00_Res/01_Images/06-01.jpg)

![06-02](README_RTUPack1-4/00_Res/01_Images/06-02.jpg)

![07-01](README_RTUPack1-4/00_Res/01_Images/07-01.jpg)

![07-02](README_RTUPack1-4/00_Res/01_Images/07-02.jpg)

![08-01](README_RTUPack1-4/00_Res/01_Images/08-01.jpg)

![08-02](README_RTUPack1-4/00_Res/01_Images/08-02.jpg)

![09-01](README_RTUPack1-4/00_Res/01_Images/09-01.jpg)

![09-02](README_RTUPack1-4/00_Res/01_Images/09-02.jpg)

![10-01](README_RTUPack1-4/00_Res/01_Images/10-01.jpg)

![10-02](README_RTUPack1-4/00_Res/01_Images/10-02.jpg)

![00-00](README_RTUPack1-4/00_Res/01_Images/00-00.jpg)

![01-01](README_RTUPack1-4/00_Res/01_Images/01-01.jpg)

![01-02](README_RTUPack1-4/00_Res/01_Images/01-02.jpg)

![02-01](README_RTUPack1-4/00_Res/01_Images/02-01.jpg)